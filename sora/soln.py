#! /usr/bin/python3
import pwn
import string

secret = "aQLpavpKQcCVpfcg" 
solution = ""

for c in secret:
    for i in string.printable:
        if ((ord(i) * 8 + 0x13) % 0x3d + 0x41) == ord(c):
            solution += i
            break

print(solution)

conn = pwn.remote('challenges.auctf.com', 30004) 
print(conn.recvuntil("key!").decode())
conn.sendline(solution)
print(conn.recvall().decode())
