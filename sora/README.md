## Sora

* **Category:** rev
* **Points:** 450 
* **Author(s):** b0uldrr
* **Date:** 4/4/20

---

### Challenge
```
This obnoxious kid with spiky hair keeps telling me his key can open all doors.
Can you generate a key to open this program before he does?

Connect to challenges.auctf.com 30004
```

### Downloads
* [sora](sora)

---

### Solution
We're given an executable file to download and the remote sever to submit our solution at. I downloaded the executable, [sora](sora), and opened it in Ghidra.

The main function prompts for user input and then passes it as a parameter to the encrypt function. If the output of encrypt() is 0, then it prints the flag.

![main](images/main.png)

The encypt function steps through every character of our input and performs the following on it `(ord(i) * 8 + 0x13) % 0x3d + 0x41)` and then compares the output against the respective value in the hardcoded string value `secret`. If the result of the encryption is equal to the secret value, then the function returns a 0 value.

![encrypt](images/encrypt.png)

We can see the value of `secret` is `aQLpavpKQcCVpfcg`.

![secret](images/secret_value.png)

I wrote a python script to brute force the required input. It loops through each character in the secret string and then performs the calculation against every printable character until it finds a match.

```
#! /usr/bin/python3
import pwn
import string

secret = "aQLpavpKQcCVpfcg" 
solution = ""

for c in secret:
    for i in string.printable:
        if ((ord(i) * 8 + 0x13) % 0x3d + 0x41) == ord(c):
            solution += i
            break

print(solution)

conn = pwn.remote('challenges.auctf.com', 30004) 
print(conn.recvuntil("key!").decode())
conn.sendline(solution)
print(conn.recvall().decode())
```
Running the script, it finds the required input to be `75y"72"b5eak"0eG`. The script then connects to the remove sever to submit the input and print the flag:

```
$ nc challenges.auctf.com 30004
Give me a key!
75y"72"b5eak"0eG
auctf{that_w@s_2_ezy_29302}
```

---

### Flag 
```
auctf{that_w@s_2_ezy_29302}
```
