## Mr. Game and Watch

* **Category:** Rev
* **Points:** 854
* **Author(s):** b0uldrr
* **Date:** 4/4/20

---

### Challenge
```
My friend is learning some wacky new interpreted language and different hashing algorithms. He's hidden a flag inside this program but I cant find it...
He told me to connect to challenges.auctf.com 30001 once I figured it out though.

Author: nadrojisk
```

### Downloads
* [java class](mr_game_and_watch.class)

---

### Solution
I downloaded the provided file, mr_game_and_watch.class. Running file on it showed it was a compiled Java class. 

```
$ file mr_game_and_watch.class 
mr_game_and_watch.class: compiled Java class data, version 55.0
```

I uploaded the file to [javadecompilers.com](javadecompilers.com) which decompiled the file in the browser. See the output, "decompiled.txt"

Reading through the source, we can see that the main function prompts for 3 inputs, runs those inputs through 3 separate encryption functions (crack_1, crack_2 and crack_3) and compares them against 3 hardcoded encrypted values (mr_game_and_watch.secret_1, 2 and 3). If our 3 inputs, post-encryption, match the hardcoded encrypted values then the program will print the flag. Our goal is to reverse-engineer the encryption functions to determine the original plaintext that we need to enter.


**Crack 1**
```
private static boolean crack_1(final Scanner scanner) {
        System.out.println("Let's try some hash cracking!! I'll go easy on you the first time. The first hash we are checking is this");
        System.out.println(invokedynamic(makeConcatWithConstants:(Ljava/lang/String;)Ljava/lang/String;, mr_game_and_watch.secret_1));
        System.out.print("Think you can crack it? If so give me the value that hashes to that!\n\t");
        return hash(scanner.nextLine(), "MD5").compareTo(mr_game_and_watch.secret_1) == 0;
} 

mr_game_and_watch.secret_1 = "d5c67e2fc5f5f155dff8da4bdc914f41";
```

The first function just uses an MD5 hash. I ran value of mr_game_and_watch.secret_1 through [crackstation.net](crackstation.net) and it found the plaintext was `masterchief`.


**Crack 2**
```
private static boolean crack_2(final Scanner scanner) {
        System.out.println("Nice work! One down, two to go ...");
        System.out.print("This next one you don't get to see, if you aren't already digging into the class file you may wanna try that out!\n\t");
        return hash(scanner.nextLine(), "SHA1").compareTo(decrypt(mr_game_and_watch.secret_2, mr_game_and_watch.key_2)) == 0;
}

private static String decrypt(final int[] array, final int n) {
        String s = "";
        for (int i = 0; i < array.length; ++i) {
            s = invokedynamic(makeConcatWithConstants:(Ljava/lang/String;C)Ljava/lang/String;, s, (char)(array[i] ^ n));
        }
        return s;
}

mr_game_and_watch.secret_2 = new int[] { 114, 118, 116, 114, 113, 114, 36, 37, 38, 38, 120, 121, 33, 36, 37, 113, 117, 118, 118, 113, 33, 117, 121, 37, 119, 34, 118, 115, 114, 120, 119, 114, 36, 120, 117, 120, 38, 114, 35, 118 };

mr_game_and_watch.key_2 = 64;
```

The second function takes our input, hashes it using SHA1, and then encrypts it by XOR'ing the individual characters with 64 (the value of mr_game_and_watch.key_2), before comparing the output against he values in in mr_game_and_watch.secret_2. To reverse this, I XOR'd the values of mr_game_and_watch.secret_2 with 64 which gave an output of `264212deff89ade15661a59e7b632872d858f2c6`. I ran this output through [crackstation.net](crackstion.net) which found the plaintext value of `princesspeach`. 


**Crack 3**
```
private static boolean crack_3(final Scanner scanner) {
        System.out.print("Nice work! Here's the last one...\n\t");
        return Arrays.equals(encrypt(hash(scanner.nextLine(), "SHA-256"), mr_game_and_watch.key_3), mr_game_and_watch.secret_3);
}

private static int[] encrypt(final String s, final int n) {
        final int[] array = new int[s.length()];
        for (int i = 0; i < s.length(); ++i) {
            array[i] = (s.charAt(i) ^ n);
        }
        return array;
}

mr_game_and_watch.secret_3 = new int[] { 268, 348, 347, 347, 269, 256, 348, 269, 256, 256, 344, 271, 271, 264, 266, 348, 257, 266, 267, 348, 269, 266, 266, 344, 267, 270, 267, 267, 348, 349, 349, 265, 349, 267, 256, 269, 270, 349, 268, 271, 351, 349, 347, 269, 349, 271, 257, 269, 344, 351, 265, 351, 265, 271, 346, 271, 266, 264, 351, 349, 351, 271, 266, 266 };

mr_game_and_watch.key_3 = 313;
```

The final crack function takes our input, hashes it with SHA-256 and then XOR's the invididual characters with 313 before comparing it to mr_game_and_watch.secret_3. To reverse this, I XOR'd the values of mr_game_and_watch.secret_3 against 313 again, which gave the output of `5ebb49e499a6613e832e433a2722edd0d2947d56fdb4d684af0f06c631fdf633`, which I ran through [crackstation.net](crackstation.net) again and it gave a plaintext value of `solidsnake`.

Here's a short python script I wrote to conduct the XOR'ing for crack2 and crack3:
```
#! /usr/bin/python3

secret2 =  [114, 118, 116, 114, 113, 114, 36, 37, 38, 38, 120, 121, 33, 36, 37, 113, 117, 118, 118, 113, 33, 117, 121, 37, 119, 34, 118, 115, 114, 120, 119, 114, 36, 120, 117, 120, 38, 114, 35, 118]
key2    = 64
value2  = ""

secret3 = [268, 348, 347, 347, 269, 256, 348, 269, 256, 256, 344, 271, 271, 264, 266, 348, 257, 266, 267, 348, 269, 266, 266, 344, 267, 270, 267, 267, 348, 349, 349, 265, 349, 267, 256, 269, 270, 349, 268, 271, 351, 349, 347, 269, 349, 271, 257, 269, 344, 351, 265, 351, 265, 271, 346, 271, 266, 264, 351, 349, 351, 271, 266, 266] 
key3    = 313
value3  = ""

for i in range(0, len(secret2)):
    value2 += chr(secret2[i] ^ key2)

for i in range(0, len(secret3)):
    value3 += chr(secret3[i] ^ key3)

print("Secret2's SHA-1 value  :", value2)
print("Secret3's SHA-256 value:", value3)
```

I then ran the remote server with those plaintext values and this gave the flag.

```
$ nc challenges.auctf.com 30001
Welcome to the Land of Interpreted Languages!
If you are used to doing compiled languages this might be a shock... but if you hate assembly this is the place to be!

Unfortunately, if you hate Java, this may suck...
Good luck!

Let's try some hash cracking!! I'll go easy on you the first time. The first hash we are checking is this
        d5c67e2fc5f5f155dff8da4bdc914f41
Think you can crack it? If so give me the value that hashes to that!
        masterchief
Nice work! One down, two to go ...
This next one you don't get to see, if you aren't already digging into the class file you may wanna try that out!
        princesspeach
Nice work! Here's the last one...
        solidsnake
That's correct!
auctf{If_u_h8_JAVA_and_@SM_try_c_sharp_2922}
```

---

### Flag 
```
auctf{If_u_h8_JAVA_and_@SM_try_c_sharp_2922}
```
