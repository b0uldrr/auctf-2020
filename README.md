## AUCTF 2020

* **CTF Time page:** https://ctftime.org/event/1020
* **Category:** Jeopardy
* **Date:** Fri, 03 April 2020, 13:00 UTC — Mon, 06 April 2020, 04:00 UTC

---

### Solved Challenges
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
|easy as pie                 |pwn       |300     |acl, permissions|
|mr game and watch           |rev       |854     |java, sha-1, sha-256, md5|
|sora                        |rev       |450     |static analysis|
|turkey                      |pwn       |889     |bof|

### Unsolved Challenges to Revisit
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
|don't break me              |pwn       |        |patch         |
|house of madness            |pwn       |        |              |
|plain jane                  |rev       |        |assembly      |
